/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.contrail.security;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.security.*;
import java.security.cert.CertificateException;




import java.security.cert.X509Certificate;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.security.auth.x500.X500Principal;

import org.bouncycastle.cert.CertIOException;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.OperatorCreationException;

/**
 *
 * @author ijj
 */
public class RootCertGen {

  private static X509Certificate createCACertificate(
    final KeyPair caKeyPair, 
    final String subjectDN, 
    final int lifetime, 
    final int serial, 
    final String sigAlgorithm)
    throws 
    IllegalArgumentException, 
    CertificateException, 
    InvalidKeyException, 
    OperatorCreationException, 
    SignatureException, 
    NoSuchAlgorithmException, 
    NoSuchProviderException, CertIOException {


    X509Certificate caCert = null;

    final boolean isCA = true;

    if (caKeyPair == null) {

      System.err.println("CA KeyPair is NULL");
    } else {

      if (caKeyPair.getPublic() == null) {
        System.err.println("CA PubKey is NULL");
      }
    }

    caCert = SecurityUtils.createCertificate(
      caKeyPair.getPublic(), subjectDN, serial,
      subjectDN, caKeyPair, sigAlgorithm, isCA,
      lifetime, 0, 0);


    caCert.verify(caKeyPair.getPublic());


    return caCert;

  }

  public static void main(String[] args) throws CertIOException {


    final String propsPath = "/etc/contrail/ca/ca.properties";

    Properties props = new Properties();
    try {
      props.load(new FileInputStream(propsPath));
    } catch (FileNotFoundException ex) {
      System.err.println(ex.getLocalizedMessage());
      System.exit(-1);
    } catch (IOException ex) {
      System.err.println(ex.getLocalizedMessage());
      System.exit(-1);

    }

    Security.addProvider(new BouncyCastleProvider());

    final String rootcaKeyPath = props.getProperty("rootcaKeyPath", "rootca.key");

    final String rcakeyAlgProp = "rootcaKeyAlgorithm";

    final String rootcaKeyAlgorithm = props.getProperty(rcakeyAlgProp, "RSA");
    final int rootcaKeyLength = Integer.valueOf(props.getProperty("rootcaKeyLength", "2048"));

    /*
     * Create the Root CA KeyPair
     *
     *
     *
     *
     */

    KeyPair rootcaKeyPair = null;
    try {
      rootcaKeyPair = SecurityUtils.generateKeyPair(rootcaKeyAlgorithm, rootcaKeyLength);
    } catch (NoSuchAlgorithmException ex) {
      System.err.println(String.format("Can't find algorithm %s - try setting %s=RSA", rootcaKeyAlgorithm, rcakeyAlgProp));
      System.exit(-1);
    }


    final int passphraseLength = Integer.valueOf(props.getProperty("passphraseLength", String.format("%s", 8)));



    final char[] keyPassphrase = SecurityUtils.getPassphrase(System.console(), passphraseLength);

    final String rootcaKeyEncAlgorithm = props.getProperty("rootcaKeyEncAlg", "DESEDE");


    /*
     * Save the Root CA KeyPair
     *
     *
     */

    try {
      SecurityUtils.writeKeyPair(rootcaKeyPath, rootcaKeyPair, keyPassphrase, rootcaKeyEncAlgorithm);
    } catch (FileNotFoundException ex) {
      Logger.getLogger(RootCertGen.class.getName()).log(Level.SEVERE, null, ex);
    } catch (IOException ex) {
      Logger.getLogger(RootCertGen.class.getName()).log(Level.SEVERE, null, ex);
    } catch (IllegalArgumentException ex) {
      Logger.getLogger(RootCertGen.class.getName()).log(Level.SEVERE, null, ex);
    }





    final String rootcaCertSubjectDN = props.getProperty("rootcaCertSubjectDN");

    if (rootcaCertSubjectDN == null) {

      System.err.println("Must specify a value for rootcaCertSubjectDN");
      System.exit(-1);

    }

    /*
     * Parse the Subject DN to check that it is valid
     *
     */

    
    try {
      new X500Principal(rootcaCertSubjectDN);
    } catch (IllegalArgumentException ex) {

      System.err.println(String.format("Error - cannot parse SubjectDN %s", rootcaCertSubjectDN));
      System.exit(-1);

    }

    final int rootcaCertLifetime = Integer.valueOf(props.getProperty("rootcaCertLlifetime", String.format("%s", 5 * 365)));

    final int rootcaCertSerial = Integer.valueOf(props.getProperty("rootcaCertSerial", String.format("%s", 1)));

    final String rootcaCertSigAlgorithm = props.getProperty("rootcaCertSigAlgorithm", "SHA256withRSA");


    /*
     * Now create the Root CA Certificate
     *
     *
     */

    X509Certificate caCert = null;

    try {

      caCert = createCACertificate(rootcaKeyPair, rootcaCertSubjectDN, rootcaCertLifetime, rootcaCertSerial, rootcaCertSigAlgorithm);

    } catch (IllegalArgumentException ex) {
      System.err.println(ex);
      System.exit(-1);
    } catch (CertificateException ex) {
      System.err.println(ex);
      System.exit(-1);
    } catch (InvalidKeyException ex) {
      System.err.println(ex);
      System.exit(-1);
    } catch (OperatorCreationException ex) {
      System.err.println(ex);
      System.exit(-1);
    } catch (SignatureException ex) {
      System.err.println(ex);
      System.exit(-1);
    } catch (NoSuchAlgorithmException ex) {
      System.err.println(ex);
      System.exit(-1);
    } catch (NoSuchProviderException ex) {
      System.err.println(ex);
      System.exit(-1);
    }


    final String caCertPath = props.getProperty("cacertPath", "rootca.crt");

    /*
     * Save the Root CA Certificate
     *
     */

    try {


      SecurityUtils.writeCertificate(new FileOutputStream(caCertPath), caCert);
    } catch (IOException ex) {
      Logger.getLogger(RootCertGen.class.getName()).log(Level.SEVERE, null, ex);
    }


  }
}
