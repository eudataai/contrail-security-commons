/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.contrail.security;

import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.cert.X509Certificate;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.X509TrustManager;
import javax.security.cert.CertificateException;

/**
 *
 * @author ijj
 */
public class MyTrustManager implements X509TrustManager {
  
  KeyStore truststore;
  String alias;

  public MyTrustManager(final KeyStore truststore, final String alias) {
    
    this.truststore = truststore;
    this.alias = alias;
    
  }

 
  public void checkClientTrusted(X509Certificate[] chain, String authType)     {
  }

  
  public void checkServerTrusted(X509Certificate[] chain, String authType)
     {
    System.out.println("checkServerTrusted");
    try {
      final X509Certificate trustedCert = (X509Certificate) truststore.getCertificate(alias);
      final X509Certificate peerCert = chain[0];

      if (!peerCert.equals(trustedCert)) {
        
      }
    } catch (KeyStoreException ex) {
      Logger.getLogger(MySSLSocketFactory.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  public X509Certificate[] getAcceptedIssuers() {
    System.out.println("getAcceptedIssuers");
    return null;
  }
};
  

