/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.contrail.security;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.*;
import java.security.cert.CertificateException;




import java.security.cert.X509Certificate;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.security.auth.x500.X500Principal;

import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.cert.CertIOException;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.OperatorCreationException;

/**
 *
 * @author ijj
 */
public class CreateHostCert {

  private static String caIssuer, caSubject;
  private static X500Name issuerOrderedX500Name;
  private static X500Principal issuerOrderedX500P;
  private static KeyPair caKeyPair;

  private static X509Certificate createCACertificate() {


    X509Certificate caCert = null;

    caIssuer = "O=Science and Technology Facilities Council,OU=Contrail Test Federation,OU=RAL-FBU,CN=Contrail Test Online CA";
//    caIssuer = "CN=Contrail Test Online CA";


    caSubject = caIssuer;

    final boolean isCA = true;

    try {

      if (caKeyPair == null) {

        System.err.println("CA KeyPair is NULL");
      } else {

        if (caKeyPair.getPublic() == null) {
          System.err.println("CA PubKey is NULL");
        }
      }

      caCert = SecurityUtils.createCertificate(
        caKeyPair.getPublic(), caSubject, 1,
        caIssuer, caKeyPair, "SHA256withRSA", isCA,
        365 * 5, 0, 0);

    } catch (CertificateException ex) {
      System.err.println(ex);
    } catch (InvalidKeyException ex) {
      System.err.println(ex);
    } catch (OperatorCreationException ex) {
      System.err.println(ex);
    } catch (CertIOException ex) {
    	System.err.println(ex);
	}

    String generatedIssuer = caCert.getIssuerDN().getName();


    X500Principal x5p = caCert.getIssuerX500Principal();
    String orderedX500Principal = SecurityUtils.reverse(x5p.toString(), ",");

    issuerOrderedX500Name = new X500Name(orderedX500Principal);

    issuerOrderedX500P = new X500Principal(orderedX500Principal);


    System.out.printf("%nX500Principal = %s.%n", x5p);

    System.out.printf("Ordered X500Principal String = %s.%n", orderedX500Principal);
    System.out.printf("%nissuerOrderedX500Name = %s.%n", issuerOrderedX500Name);

    System.out.printf("%nissuerOrderedX500P = %s.%n", issuerOrderedX500P);






    String generatedSubject = caCert.getSubjectDN().getName();

    assert (generatedIssuer == generatedSubject);



    try {

      String subjKIDStr = SecurityUtils.getExtensionValueAsOctetString(caCert, "2.5.29.14");//NOPMD
      String authKIDStr = SecurityUtils.getExtensionValueAsOctetString(caCert, "2.5.29.35");//NOPMD      

    } catch (IOException ex) {
      System.err.println(ex);
    }

    try {

      System.out.println("CA Root Cert:%n");
      SecurityUtils.writeCertificate(System.out, caCert);

      System.out.println("CA Keypair%n");
      SecurityUtils.writeKey(System.out, caKeyPair.getPrivate());

    } catch (IOException ex) {
      System.err.println(ex);
    }


//    System.err.printf("subjIDStr = %s.%n", subjKIDStr);
//    System.err.printf("authIDStr = %s.%n", authKIDStr);

    try {

      caCert.verify(caKeyPair.getPublic());

//      System.err.println("Root cert verified OK");

      boolean[] keysBits = caCert.getKeyUsage();

      if (keysBits != null) {

        for (int i = 0; i < keysBits.length; i++) {
//          System.err.printf("%b\t", keysBits[i]);
        }
        //       System.err.println();

      }

      List<String> ekus = caCert.getExtendedKeyUsage();

      if (ekus != null) {

        for (String ku : ekus) {
//          System.err.printf("%s\t", ku);
        }

      }


    } catch (CertificateException ex) {
      System.err.println(ex);
    } catch (NoSuchAlgorithmException ex) {
      System.err.println(ex);
    } catch (InvalidKeyException ex) {
      System.err.println(ex);
    } catch (NoSuchProviderException ex) {
      System.err.println(ex);
    } catch (SignatureException ex) {
      System.err.println(ex);
    }

    return caCert;

  }
  
  public static void printUsage() {
    
    System.err.printf("Usage: gen-host-cert caKey caCert hostname [lifetime_days]");    
  
  }

  public static void main(String[] args) {


    if (args.length != 3 && args.length != 4) {

      printUsage();
      
      System.exit(-1);

    }
    
    Security.addProvider(new BouncyCastleProvider());

    final String caKeyPath = args[0];
    final String caCertPath = args[1];
    final String hostname = args[2];
    
    int days = 30;
    
    if (args.length == 4) {
    
      final String duration = args[3];
      
      try {
        
        days = Integer.valueOf(duration);
        
      } catch (NumberFormatException ex) {
        
        System.err.println(ex);
        printUsage();
        
        System.exit(-1);
      }
    
    }
    
    BigInteger serialNumber = BigInteger.valueOf(System.currentTimeMillis());

    try {
      
      final PrivateKey caKey = SecurityUtils.readPrivateKey(caKeyPath, null);
      final X509Certificate caCert = SecurityUtils.getCertFromStream(new FileInputStream(caCertPath));

      KeyPair eeKeyPair = SecurityUtils.generateKeyPair("RSA", 2048);

      final X509Certificate eeCert = SecurityUtils.createHostCertificate(
        eeKeyPair.getPublic(), hostname, serialNumber, caCert,
        caKey, "SHA256withRSA", null,
        days, 0, 0);
      
      final String keyOutPath = hostname + "-key.pem";
      
      final String certOutPath = hostname + "-cert.pem";
      
      SecurityUtils.writeKey(new FileOutputStream(new File(keyOutPath)), eeKeyPair.getPrivate());
      System.err.printf("Write private key to %s.%n", keyOutPath);
      
      SecurityUtils.writeCertificate(new FileOutputStream(new File(certOutPath)), eeCert);
      System.err.printf("Write cert to %s.%n", certOutPath);

    } catch (OperatorCreationException ex) {
      System.err.println(ex);
      System.exit(-1);
    } catch (CertificateException ex) {
      System.err.print(ex);
      System.exit(-1);

    } catch (InvalidKeyException ex) {
      System.err.println(ex);
      System.exit(-1);
    } catch (FileNotFoundException ex) {
      Logger.getLogger(CreateHostCert.class.getName()).log(Level.SEVERE, null, ex);
      System.exit(-1);
    } catch (IOException ex) {
      Logger.getLogger(CreateHostCert.class.getName()).log(Level.SEVERE, null, ex);
      System.exit(-1);
    } catch (NoSuchAlgorithmException ex) {
      Logger.getLogger(CreateHostCert.class.getName()).log(Level.SEVERE, null, ex);
      System.exit(-1);
    }




  }
}
