/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.contrail.security;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.*;
import java.security.cert.X509Certificate;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.SSLContext;

import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import javax.security.cert.CertificateException;
import org.apache.http.conn.ssl.SSLSocketFactory;

/**
 *
 * @author ijj
 */
public class MySSLSocketFactory extends SSLSocketFactory {

  SSLContext sslContext = SSLContext.getInstance("TLS");
  KeyStore truststore;
  String alias;

  public MySSLSocketFactory(final KeyStore truststore, final String alias)
    throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, UnrecoverableKeyException {
    super(truststore);

    final KeyStore _truststore = truststore;
    final String _alias = alias;

    //       X509Certificate trustedCert;


    System.out.println("MySSLSocketFactory()");

    X509TrustManager tm = new X509TrustManager() {
      @Override
      public void checkClientTrusted(X509Certificate[] xcs, String string) throws java.security.cert.CertificateException {
        throw new UnsupportedOperationException("Not supported yet.");
      }

      @Override
      public void checkServerTrusted(X509Certificate[] xcs, String string) throws java.security.cert.CertificateException {
        
        System.out.printf("checkServerTrusted: Peer sent %d certificates.", xcs);
        
        X509Certificate trustedCert = null;
        X509Certificate peerCert = null;
        
        try {
          
          trustedCert = (X509Certificate) _truststore.getCertificate(_alias);
          
          peerCert = xcs[0];

          if (!peerCert.equals(trustedCert)) {
            throw new java.security.cert.CertificateException("Server certificate not recognised");
          }
          
        } catch (KeyStoreException ex) {
          Logger.getLogger(MySSLSocketFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        peerCert.checkValidity();

      }

      @Override
      public X509Certificate[] getAcceptedIssuers() {
        throw new UnsupportedOperationException("Not supported yet.");
      }
    };

     

        sslContext.init(null /*  KeyManager[] */, new TrustManager[] { tm }, null);
    }

    @Override
    public Socket createSocket(Socket socket, String host, int port, boolean autoClose) throws IOException, UnknownHostException {
        return sslContext.getSocketFactory().createSocket(socket, host, port, autoClose);
    }

    @Override
    public Socket createSocket() throws IOException {
        return sslContext.getSocketFactory().createSocket();
    }
}
