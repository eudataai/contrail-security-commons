/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.contrail.security;

import java.io.IOException;
import java.security.cert.X509Certificate;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.scheme.SchemeSocketFactory;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.X509HostnameVerifier;


import org.apache.http.impl.client.DefaultHttpClient;

/**
 *
 * @author ijj
 */
/*
 This code is public domain: you are free to use, link and/or modify it in any way you want, for all purposes including commercial applications. 
 */
public class WebClientDevWrapper {

  public static DefaultHttpClient wrapClient(final DefaultHttpClient base, final int port,
    final String ksPath, final String ksPassphrase) {

    try {

      SSLContext ctx = SSLContext.getInstance("TLS");

      X509TrustManager tm;

      tm = new DefaultingTrustManager(ksPath, ksPassphrase);


      X509HostnameVerifier verifier = new X509HostnameVerifier() {
        @Override
        public void verify(String targetHostname, SSLSocket ssls) throws IOException {
        }

        @Override
        public void verify(String string, X509Certificate xc) throws SSLException {
          System.out.println("String, X509Certificate");
        }

        @Override
        public void verify(String string, String[] strings, String[] strings1) throws SSLException {
          System.out.println("String, String[], String[]");
        }

        @Override
        public boolean verify(String string, SSLSession ssls) {
          System.out.println("String, SSLSession");
          return true;
        }
      };

      ctx.init(null, new TrustManager[]{tm}, null);
      SchemeSocketFactory ssf = new SSLSocketFactory(ctx);
      //ssf.setHostnameVerifier(verifier);
      ClientConnectionManager ccm = base.getConnectionManager();
      SchemeRegistry sr = ccm.getSchemeRegistry();
      sr.register(new Scheme("https", port, ssf));

      return new DefaultHttpClient(ccm, base.getParams());
    } catch (Exception ex) {
      ex.printStackTrace();
      return null;
    }
  }
}
