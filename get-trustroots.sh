#!/bin/sh

m2_repo=/Users/ianjohnson/.m2/repository


classpath=${m2_repo}/org/apache/httpcomponents/httpcore/4.1.2/httpcore-4.1.2.jar
classpath=${classpath}:${m2_repo}/org/apache/httpcomponents/httpclient/4.1.2/httpclient-4.1.2.jar
classpath=${classpath}:${m2_repo}/commons-logging/commons-logging/1.1.1/commons-logging-1.1.1.jar
classpath=${classpath}:${m2_repo}/commons-codec/commons-codec/1.6/commons-codec-1.6.jar
classpath=${classpath}:${m2_repo}/security-commons/security-commons/0.1/security-commons-0.1.jar
classpath=${classpath}:${m2_repo}/bouncycastle/bcprov-jdk16/140/bcprov-jdk16-140.jar


#
# Set the following variables if your site has an outgoing web proxy
#

proxyHost=wwwcache.rl.ac.uk
proxyPort=8080


#
# Set the following to true unless you are sure you have IPv6 connectivity to the destination host
#

preferIPv4=true



java -Dhttp.proxyHost=${proxyHost} -Dhttp.proxyPort=${proxyPort} -Djava.net.preferIPv4Stack=${preferIPv4} -cp target/federationcaclient-0.1.jar:${classpath} eu.contrail.federation.security.ca.client.GetTrustroots $*
