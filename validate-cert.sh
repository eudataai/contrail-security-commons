#!/bin/bash

jar_dir=${HOME}/lib

jars=(\
httpcore-4.1.2.jar \
httpclient-4.1.2.jar \
commons-lang-2.6.jar \
commons-logging-1.1.1.jar \
commons-codec-1.6.jar \
bcprov-jdk16-146.jar \
bcmail-jdk16-146.jar \
java-getopt-1.0.13.jar \
security-commons-1.0-SNAPSHOT.jar \
)

classpath=

for jar in ${jars[@]}

do
  classpath=${classpath}:${jar_dir}/${jar}
done

echo ${classpath}
 

#
# Set the variable PROXY_OPTS if your site has an outgoing web proxy
#

#
# Should we pick up the value of 'http_proxy' from the environment, first?
#

#PROXY_OPTS="-Dhttp.proxyHost=wwwcache.rl.ac.uk -Dhttp.proxyPort=8080"

#
# Set the following to true unless you are sure you have IPv6 connectivity to the destination host
#
# If you wish to use IPv6, comment out this setting

IPV4_OPT="-Djava.net.preferIPv4Stack=true"
#DBG_OPT=-Djavax.net.debug=all
java ${DBG_OPT}  ${PROXY_OPTS} ${IPV4_OPT} -cp ${classpath} eu.contrail.security.CertificatePathValidator $* 

exit $?
